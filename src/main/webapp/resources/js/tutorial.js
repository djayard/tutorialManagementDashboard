tmdApp.controller('tutorialsCtrl', function($scope,$http,$location)
		{
			$scope.loaded = function()
			{
				document.getElementById("signoutButton").classList.remove("hidden")
				document.getElementById("tutorials").classList.remove("unselected")
				document.getElementById("tutorials").classList.add("selected")
				document.getElementById("tutorialsTable").classList.remove("hidden")
				document.getElementById("searchBar").classList.remove("hidden")
				
				$scope.sortType = 'creationDate'; $scope.sortOrder = false;
				
				$scope.prepareTutorials();
			}
			
			$scope.editSort = function()
			{
				if( isNaN($scope.sortChoice) )
					return
					
				var choice = $scope.sortOptions[$scope.sortChoice];
				
				$scope.sortType = choice.sort
				$scope.sortOrder =  choice.order
			}
			
			$scope.prepareTutorials = function()
			{	
				$http.get("service/tutorials", {headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.tutorials = data;
						}
				)
				
			}
			
			$scope.search = function(query)
			{
				if( query.trim().length <= 0 )
				{
					$scope.prepareTutorials();
					return;
				}
					
				$http.get("service/tutorialSearch?query=" + query, {headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.tutorials = data
						}
				)
			}
			
			$scope.viewTutorial = function(tutorial)
			{
				$location.path("previewTutorial/" + tutorial.id)
			}
			
			$scope.editTutorial = function(tutorial)
			{
				$location.path("editTutorial/" + tutorial.id)
			}
			
			$scope.deleteTutorial = function(tutorial)
			{
				$http({method: 'DELETE', 
					url: "service/deleteTutorial?id=" + tutorial.id,
					headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.tutorials = data;
						}
				)
			}
		}
)

tmdApp.controller("editTutorialCtrl", function($scope,$http,$location,$routeParams)
		{
			$scope.loaded = function()
			{
				
				document.getElementById("signoutButton").classList.remove("hidden")
				$scope.sections = new Array();
				$scope.add = {};
				
				$scope.editMode = !isNaN($routeParams.id)
				if(  $scope.editMode )
					$scope.fetchProperties();
				
				
				document.getElementById("editTutorial").classList.remove("hidden")
			}
			
			$scope.editSort = function()
			{
				if( isNaN($scope.sortChoice) )
					return
					
				var choice = $scope.sortOptions[$scope.sortChoice];
				
				$scope.sortType = choice.sort
				$scope.sortOrder =  choice.order
			}
			
			$scope.search = function(query)
			{
				if( query.trim().length <= 0 )
				{
					return;
				}
					
				$http.get("service/sectionSearch?query=" + query, {headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.allSections = $scope.trim(data);
						}
				)
			}
			
			$scope.trim = function(data)
			{
				$scope.sections.forEach(function(element)
						{
	
							var i = 0; j = data.length - 1;
							var index;
							while( i <= j )
							{
								index = Math.floor((i+j) / 2);
								
								if( data[index].id == element.id )
								{
									data.splice(index,1)
									return
								}
								else if( data[index].id < element.id)
									i = index + 1
								else
									j = index - 1
								
							}
					
							
						}
				)
				return data
			}
			
			$scope.fetchProperties = function()
			{
				
				$http.get("service/getTutorial?id=" + $routeParams.id, {headers: {Accept: 'application/json'}})
				.success(function(data)
						{
							$scope.title = data.title;
							$scope.description = data.description;
							$scope.bucket = data.bucket;
							$scope.fileKey = data.fileKey;
							$scope.tags = data.tags;
							$scope.sections = data.sections;
						}
				)
			}
			
			$scope.createTutorial = function()
			{
				var sectionIds = new Array();
				for(var i = 0; i < $scope.sections.length; ++i)
				{
					sectionIds.push($scope.sections[i].id)
				}
				
				var tutorial = {title: $scope.title, description: $scope.description,
						bucket: $scope.bucket, fileKey: $scope.fileKey, tags: $scope.tags,
						sections: sectionIds};
				
				if( $scope.editMode )
				{
					tutorial.id = $routeParams.id;
					$http.post("service/updateTutorial", tutorial, {headers: {Accept: "text/plain"}})
					.success(function(data)
							{
								$location.path("tutorials")
							}
					)
				}
				else
				{
					$http.post("service/createTutorial", tutorial, {headers: {Accept: 'text/plain'}})
					.success(function()
							{
								$location.path("tutorials")
							}
					)
				}
				
				
			}
			
			$scope.selectSection = function()
			{
				
				$http.get("service/sections", {headers: {Accept: 'application/json'}})
				.success(function(data)
						{
							$scope.allSections = $scope.trim(data);
						}
				)
				
				
			}
			
			$scope.addSections = function()
			{
				
				$scope.allSections.forEach(function(element)
						{
							if( $scope.add[element.id] == true )
								$scope.sections.push({id: element.id, title: element.title});
							
							$scope.add[element.id] = false;
						}
				)
				
				
			}
			
			$scope.removeSection = function()
			{
				var select = document.getElementById("selectedSections")
				var index = select.selectedIndex
				
				if( index >= 0 )
					$scope.sections.splice(index,1);
			}
			
			$scope.moveUp = function()
			{
				var select = document.getElementById("selectedSections")
				var index = select.selectedIndex
				
				if( index >= 0 )
					if(  index == 0)
					{
						var element = $scope.sections.shift();
						$scope.sections.push(element);
					}
					else
					{
						var element = $scope.sections[index]
						$scope.sections.splice(index,1)
						$scope.sections.splice(index-1,0,element)
					}
				
			}
			
			$scope.moveDown = function()
			{
				var select = document.getElementById("selectedSections")
				var index = select.selectedIndex
				
				if( index >= 0 )
					if( index == $scope.sections.length - 1)
					{
						var element = $scope.sections.pop()
						$scope.sections.unshift(element)
					}
					else
					{
						var element = $scope.sections[index]
						$scope.sections.splice(index,1)
						$scope.sections.splice(index+1, 0, element)
					}
			}
		}
)

tmdApp.controller("previewTutorialCtrl", function($scope,$http,$location,$routeParams)
		{
			$scope.loaded = function()
			{
				document.getElementById("signoutButton").classList.remove("hidden")
				document.getElementById("preview").classList.remove("hidden")
				
				$scope.fetchProperties()
				$scope.showPage();
			}
			
			$scope.fetchProperties = function()
			{
				var id = $routeParams.id;
				
				$http.get("service/getTutorial?id=" + id, {headers: {Accept: 'application/json'}})
				.success(function(data)
						{
							$scope.title = data.title;
							$scope.description = data.description;
							$scope.bucket = data.bucket;
							$scope.fileKey = data.fileKey;
							$scope.tags = data.tags;
							$scope.sections = data.sections;
							$scope.creationDate = data.creationDate
						}
				)
			}
			
			$scope.showPage = function()
			{
				if( ! isNaN($routeParams.sectionId) )
					$http.get("service/viewSection?id=" + $routeParams.sectionId)
					.success(function(data)
							{
								document.getElementById("mainPreviewContent").innerHTML = data;
							}
					)	
				else
					$http.get("service/viewTutorial?id=" + $routeParams.id)
					.success(function(data)
							{
								document.getElementById("mainPreviewContent").innerHTML = data;
							}
					)
			}
			
			$scope.getSectionView2 = function(section)
			{
				$location.path("previewTutorial/" + $routeParams.id + '/' + section.id)
			}
		}
)