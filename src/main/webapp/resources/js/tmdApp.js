var tmdApp = angular.module("tmdApp", ["ngRoute"]);
var baseUrl = "/TMD/resources/html/";
var mainHtml = baseUrl + "main.html";

tmdApp.config(function ($routeProvider)
	{
		$routeProvider
		.when('/tutorials', {templateUrl: mainHtml, controller: "tutorialsCtrl"})
		.when('/sections', {templateUrl: mainHtml, controller: "sectionsCtrl"})
		.when('/editTutorial/:id', {templateUrl: mainHtml, controller: "editTutorialCtrl"})
		.when('/editSection/:id', {templateUrl: mainHtml, controller: "editSectionCtrl"})
		.when('/previewTutorial/:id/:sectionId?', {templateUrl: mainHtml, controller: "previewTutorialCtrl"})
		.when('/previewSection/:id', {templateUrl: mainHtml, controller: "previewSectionCtrl"})
		.otherwise({templateUrl: baseUrl + 'login.html', controller: "loginCtrl"});
	}

)

tmdApp.controller("loginCtrl", function($scope,$http,$location)
	{
		var signOut = document.getElementById("signoutButton");
		if( signOut )
			signOut.classList.add("hidden")
		
		$scope.attemptLogin = function()
		{
			
			$http.post("login", 
					"username=" + $scope.username + "&password=" + $scope.password, 
					{headers : {'Content-Type': 'application/x-www-form-urlencoded'}})
			.success(function(data)
					{
						if( data == true)
						{
							$location.path("tutorials");
						}
						else
							alert("Login Failed");
					}
			)
			
		}
	
	}	
)

tmdApp.run(function($rootScope, $http, $location)
		{
			$rootScope.logout = function()
			{
				$http.get("logout", {headers: {Accept: "text/plain"}})
				.success(function()
					{
						$location.path("");
					}
				)
			}
			
			$rootScope.sortOptions = 
			[
				{sort: 'title', order: false},
				{sort: 'title', order: true},
				{sort: 'creationDate', order: false},
				{sort: 'creationDate', order: true},
				{sort: 'lastModifiedDate', order: false},
				{sort: 'lastModifedDate', order: true}		
			]
			
			
		}
)
