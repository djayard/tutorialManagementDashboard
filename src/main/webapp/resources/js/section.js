tmdApp.controller("sectionsCtrl", function($scope,$http,$location)
		{
			$scope.loaded = function()
			{
				document.getElementById("signoutButton").classList.remove("hidden")
				document.getElementById("sections").classList.remove("unselected")
				document.getElementById("sections").classList.add("selected")
				document.getElementById("sectionsTable").classList.remove("hidden")
				document.getElementById("searchBar").classList.remove("hidden")
				
				$scope.prepareSections();
			}
			
			$scope.editSort = function()
			{
				if( isNaN($scope.sortChoice) )
					return
					
				var choice = $scope.sortOptions[$scope.sortChoice];
				
				$scope.sortType = choice.sort
				$scope.sortOrder =  choice.order
			}
			
			$scope.search = function(query)
			{
				if( query.trim().length <= 0 )
				{
					$scope.prepareSections();
					return;
				}
					
				$http.get("service/sectionSearch?query=" + query, {headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.sections = data
						}
				)
			}
			
			$scope.prepareSections = function()
			{	
				$http.get("service/sections", {headers: {Accept: "application/json"}})
				.success(function(data)
						{
							$scope.sections = data;
						}
				)
				
			}
			
			$scope.viewSection = function(section)
			{
				$location.path("previewSection/" + section.id)
			}
			
			$scope.editSection = function(section)
			{
				$location.path("editSection/" + section.id)
			}
			
			$scope.deleteSection = function(section)
			{
				$http({method: 'DELETE', 
					url: 'service/deleteSection?id=' + section.id, 
					headers: {Accept: "application/json"} })
				.success(function(data)
						{
							$scope.sections = data;
						}
				)
			}
			
			
		}

)

tmdApp.controller("editSectionCtrl", function($scope,$http,$location,$routeParams)
		{
			$scope.loaded = function()
			{
				document.getElementById("signoutButton").classList.remove("hidden")
				$scope.editMode = ! isNaN($routeParams.id)
				
				if( $scope.editMode )
					$scope.fetchProperties();
				document.getElementById("editSection").classList.remove("hidden")	
				
				
			}
			
			$scope.fetchProperties = function()
			{
				$http.get("service/getSection?id=" + $routeParams.id, {headers: {Accept: 'application/json'}})
				.success(function(data)
						{
							$scope.title = data.title;
							$scope.description = data.description;
							$scope.bucket = data.bucket;
							$scope.fileKey = data.fileKey;
							$scope.tags = data.tags;
						}
				)
			}
			
			$scope.createSection = function()
			{
				
				var section = {title: $scope.title, description: $scope.description,
						tags: $scope.tags, bucket: $scope.bucket, fileKey: $scope.fileKey}
				if( $scope.editMode )
				{
					section.id = $routeParams.id
					$http.post("service/updateSection", section, {headers: {Accept: 'text/plain'}})
					.success(function()
							{
								$location.path("sections")
							}
					)
				}
				else
					$http.post("service/createSection", section, {headers: {Accept: 'text/plain'}})
					.success(function()
							{
								$location.path("sections")
							}
					)
				
			}
	
	
		}
)

tmdApp.controller("previewSectionCtrl", function($scope,$http,$location,$routeParams)
		{
			$scope.loaded = function()
			{
				document.getElementById("signoutButton").classList.remove("hidden")
				document.getElementById("preview").classList.remove("hidden")
				
				$scope.showPage();
			}
			
			$scope.showPage = function()
			{
				$http.get("service/viewSection?id=" + $routeParams.id)
				.success(function(data)
						{
							document.getElementById("mainPreviewContent").innerHTML = data;
						}
				)
			}
	}
)