package tmd.dataAccess;

import java.util.List;

import tmd.dataAccess.models.Section;
import tmd.dataAccess.models.Tutorial;
import tmd.dataAccess.models.UserTmd;

public interface DaoForTmd {
	
	void dummy();
	UserTmd login(String username, String password);
	UserTmd findUser(String username);
	
	List<Tutorial> getTutorials();
	List<Section> getSections();
	
	List<Tutorial> searchTutorials(String query);
	List<Section> searchSections(String query);
	
	String[] getS3TutorialInfo(Integer id);
	String[] getS3SectionInfo(Integer id);
	
	boolean saveTutorial(Tutorial tutorial);
	boolean saveSection(Section section);
	
	Tutorial getTutorial(Integer id);
	Section getSection(Integer id);
	
	boolean updateTutorial(Tutorial tutorial);
	boolean updateSection(Section section);
	
	boolean deleteTutorial(Integer id);
	boolean deleteSection(Integer id);
	
	

}
