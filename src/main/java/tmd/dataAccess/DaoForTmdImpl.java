package tmd.dataAccess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import tmd.dataAccess.models.*;

@Transactional
public class DaoForTmdImpl implements DaoForTmd{
	
	private SessionFactory sf;
	private PasswordEncoder encoder;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf)
	{
		this.sf = sf;
	}
	
	@Autowired
	public void setPasswordEncoder(PasswordEncoder encoder)
	{
		this.encoder = encoder;
	}

	public void dummy() {
		Session session = sf.getCurrentSession();
		
		if( session != null )
			System.err.println(session.isConnected());
		
	}

	public UserTmd login(String username, String password) {
		Session session = sf.getCurrentSession();
		
		UserTmd user = (UserTmd)
				session.createQuery("from tmd.dataAccess.models.UserTmd where username = :username")
				.setString("username", username).uniqueResult();
		
		if( user != null )
		{
			if( !  encoder.matches(password, user.getPassword())  )
				user = null;
			else
			{
				session.evict(user);
				user.setPassword(null);
			}
		}
		
		return user;
		
	}

	public List<Tutorial> getTutorials() {
		Session session = sf.getCurrentSession();
		
		List<Tutorial> tutorials = session.createQuery("from tmd.dataAccess.models.Tutorial").list();
		
		return tutorials;
	}

	public String[] getS3TutorialInfo(Integer id) {
		Session session = sf.getCurrentSession();
		Tutorial tutorial = (Tutorial) session.get(Tutorial.class, id);
		
		String[] result = null;
		if( tutorial != null )
		{
			result = new String [2];
			result[0] = tutorial.getBucket();
			result[1] = tutorial.getFileKey();
		}
		
		return result;
		
	}
	
	public String[] getS3SectionInfo(Integer id) {
		Session session = sf.getCurrentSession();
		Section section = (Section) session.get(Section.class, id);
		
		String[] result = null;
		if( section != null )
		{
			result = new String [2];
			result[0] = section.getBucket();
			result[1] = section.getFileKey();
		}
		
		return result;
		
	}

	public UserTmd findUser(String username) {
		Session session = sf.getCurrentSession();
		
		return (UserTmd) session
				.createQuery("from tmd.dataAccess.models.UserTmd where username = :username")
				.setString("username", username).uniqueResult();
	}

	public List<Section> getSections() {
		Session session = sf.getCurrentSession();
		
		List<Section> sections = session.createQuery("from tmd.dataAccess.models.Section order by id").list();
		
		return sections;
	}

	public boolean saveTutorial(Tutorial tutorial) {
		Session session = sf.getCurrentSession();
		
		Set<Tag> tags = new HashSet<Tag>();
		for(Tag t: tutorial.getTags())
		{
			Tag obj = (Tag) session
					.createQuery("from tmd.dataAccess.models.Tag "
							+ "where description = :description")
					.setString("description", t.getDescription()).uniqueResult();
			
			if( obj == null )
				session.save(t);
			else
				t = obj;
			
			tags.add(t);
		}
		
		List<Section> sections = new ArrayList<Section>();
		for(Section s: tutorial.getSections())
		{
			sections.add((Section) session.get(Section.class, s.getId()));
		}
		
		tutorial.setSections(sections);
		tutorial.setTags(tags);
		session.save(tutorial);
		
		return true;
	}

	public boolean saveSection(Section section) {
		Session session = sf.getCurrentSession();
		
		Set<Tag> tags = new HashSet<Tag>();
		for(Tag t: section.getTags())
		{
			Tag obj = (Tag) session
					.createQuery("from tmd.dataAccess.models.Tag "
							+ "where description = :description")
					.setString("description", t.getDescription()).uniqueResult();
			
			if( obj == null )
				session.save(t);
			else
				t = obj;
			
			tags.add(t);
		}
		
		
		
		
		section.setTags(tags);
		session.save(section);

		return true;
	}

	public Tutorial getTutorial(Integer id)
	{
		Session session = sf.getCurrentSession();
		
		Tutorial tutorial = (Tutorial) session.get(Tutorial.class, id);
		
		return tutorial;
	}
	
	public Section getSection(Integer id) {
		Session session = sf.getCurrentSession();
		
		Section section = (Section) session.get(Section.class, id);
		
		return section;
	}

	public boolean updateTutorial(Tutorial tutorial) 
	{
		Session session = sf.getCurrentSession();
		
		Set<Tag> tags = new HashSet<Tag>();
		Query tagQuery = session.createQuery("from tmd.dataAccess.models.Tag "
				+ "where description = :description");
		for(Tag t: tutorial.getTags())
		{
			Tag obj = (Tag) tagQuery.setString("description", t.getDescription()).uniqueResult();
			
			if( obj == null )
				session.save(t);
			else
				t = obj;
			
			tags.add(t);
		}
		
		List<Section> sections = new ArrayList<Section>();
		for(Section s: tutorial.getSections())
		{
			sections.add((Section) session.get(Section.class, s.getId()));
		}
		
		Tutorial orig = (Tutorial) session.get(Tutorial.class, tutorial.getId());
		
		orig.setBucket(tutorial.getBucket());
		orig.setDescription(tutorial.getDescription());
		orig.setFileKey(tutorial.getFileKey());
		orig.setLastModifiedDate(tutorial.getLastModifiedDate());
		orig.setSections(sections);
		orig.setTags(tags);
		orig.setTitle(tutorial.getTitle());
		
		return true;
	}
	
	public boolean updateSection(Section section) 
	{
		Session session = sf.getCurrentSession();
		
		Set<Tag> tags = new HashSet<Tag>();
		Query tagQuery = session.createQuery("from tmd.dataAccess.models.Tag "
				+ "where description = :description");
		for(Tag t: section.getTags())
		{
			Tag obj = (Tag) tagQuery.setString("description", t.getDescription()).uniqueResult();
			
			if( obj == null )
				session.save(t);
			else
				t = obj;
			
			tags.add(t);
		}
		
		Section orig = (Section) session.get(Section.class, section.getId());
		
		orig.setBucket(section.getBucket());
		orig.setDescription(section.getDescription());
		orig.setFileKey(section.getFileKey());
		orig.setLastModifiedDate(section.getLastModifiedDate());
		orig.setTags(tags);
		orig.setTitle(section.getTitle());
		
		return true;
	}

	public boolean deleteTutorial(Integer id) 
	{
		Session session = sf.getCurrentSession();
		
		session.delete(session.get(Tutorial.class, id));
		
		return true;
		
	}

	public boolean deleteSection(Integer id) 
	{
		Session session = sf.getCurrentSession();
		
		session.createSQLQuery("delete from TUTORIAL_SECTIONS where SECTION_ID = :id")
		.setInteger("id", id).executeUpdate();
		
		session.delete(session.get(Section.class, id));
		
		return true;
	}

	public List<Tutorial> searchTutorials(String query) {
		Session session = sf.getCurrentSession();
		
		List<Tutorial> tutorials = session.createCriteria(Tutorial.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.createAlias("tags", "t", Criteria.LEFT_JOIN)
				.add(Restrictions.disjunction()
						.add(Restrictions.ilike("title", query))
						.add(Restrictions.ilike("t.description", query))
					)
				.addOrder(Order.asc("id")).list();
		
		for(Tutorial s : tutorials)
		{
			Hibernate.initialize(s.getTags());
		}
		
		return tutorials;
	}

	public List<Section> searchSections(String query) {
		Session session = sf.getCurrentSession();
		
		List<Section> sections = session.createCriteria(Section.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.createAlias("tags", "t", Criteria.LEFT_JOIN)
				.add(Restrictions.disjunction()
						.add(Restrictions.ilike("title", query))
						.add(Restrictions.ilike("t.description", query))
					)
				.addOrder(Order.asc("id")).list();
		
		for(Section s : sections)
		{
			Hibernate.initialize(s.getTags());
		}
		
		return sections;
		
	}
	
	
	
	

}
