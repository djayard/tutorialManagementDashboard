package tmd.dataAccess.models;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="TUTORIAL")
public class Tutorial {

	@Id
	@Column(name="TUTORIAL_ID")
	@SequenceGenerator(name="tutorialGen", sequenceName="TUTORIAL_SEQ")
	@GeneratedValue(generator="tutorialGen", strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="S3BUCKET")
	private String bucket;
	
	@Column(name="S3FILE_KEY")
	private String fileKey;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="DATE_CREATED")
	private Date creationDate;
	
	@Column(name="DATE_MODIFIED")
	private Date lastModifiedDate;
	
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="TUTORIAL_SECTIONS", joinColumns={@JoinColumn(name="TUTORIAL_ID")}, inverseJoinColumns={@JoinColumn(name="SECTION_ID")})
	@OrderColumn(name="SECTION_ORDER", updatable=true)
	private List<Section> sections;

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="TUTORIAL_TAGS", joinColumns={@JoinColumn(name="TUTORIAL_ID")}, inverseJoinColumns={@JoinColumn(name="TAG_ID")})
	private Set<Tag> tags;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getFileKey() {
		return fileKey;
	}

	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return "Tutorial [id=" + id + ", title=" + title + ", bucket=" + bucket + ", fileKey=" + fileKey
				+ ", description=" + description + ", creationDate=" + creationDate + ", lastModifiedDate="
				+ lastModifiedDate + ", sections=" + sections + ", tags=" + tags + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bucket == null) ? 0 : bucket.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((fileKey == null) ? 0 : fileKey.hashCode());
		result = prime * result + ((lastModifiedDate == null) ? 0 : lastModifiedDate.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tutorial other = (Tutorial) obj;
		if (bucket == null) {
			if (other.bucket != null)
				return false;
		} else if (!bucket.equals(other.bucket))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (fileKey == null) {
			if (other.fileKey != null)
				return false;
		} else if (!fileKey.equals(other.fileKey))
			return false;
		if (lastModifiedDate == null) {
			if (other.lastModifiedDate != null)
				return false;
		} else if (!lastModifiedDate.equals(other.lastModifiedDate))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	
}
