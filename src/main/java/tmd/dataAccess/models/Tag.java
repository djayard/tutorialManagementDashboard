package tmd.dataAccess.models;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="TAGS")
public class Tag {
	
	@Id
	@Column(name="TAG_ID")
	@SequenceGenerator(name="tagGen", sequenceName="TAGS_SEQ")
	@GeneratedValue(generator="tagGen", strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@ManyToMany(mappedBy="tags")
	private Set<Tutorial> tutorials;
	
	@ManyToMany(mappedBy="tags")
	private Set<Section> sections;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Tutorial> getTutorials() {
		return tutorials;
	}

	public void setTutorials(Set<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}

	public Set<Section> getSections() {
		return sections;
	}

	public void setSections(Set<Section> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", description=" + description + ", tutorials=" + tutorials + ", sections=" + sections
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	
	

}
