package tmd.spring.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.security.AWSCredentials;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import tmd.dataAccess.DaoForTmd;
import tmd.dataAccess.DaoForTmdImpl;

@EnableWebMvc
@EnableTransactionManagement
@Configuration
@ComponentScan({"tmd.spring.rest"})
public class TmdSpringConfig extends WebMvcConfigurerAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		
		return viewResolver;
	}
	
	@Bean
	public PasswordEncoder encoder()
	{
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DataSource dataSource()
	{
		BasicDataSource source = new BasicDataSource();
		source.setDriverClassName("oracle.jdbc.OracleDriver");
		source.setUrl("jdbc:oracle:thin:@tmd.cud6njtkx96k.us-east-1.rds.amazonaws.com:1521:tmd");
		source.setUsername("denny");
		source.setPassword("dennypass");
		
		return source;
	}
	
	@Bean
	public FactoryBean<SessionFactory> sessionFactory(@Autowired DataSource src)
	{
		AnnotationSessionFactoryBean sf = new AnnotationSessionFactoryBean();
		sf.setDataSource(src);
		sf.setPackagesToScan("tmd.dataAccess.models");
		Properties dialect = new Properties();
		dialect.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		//dialect.setProperty("hibernate.show_sql", "true");
		sf.setHibernateProperties(dialect);
		
		return sf;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(@Autowired FactoryBean<SessionFactory> sf)
	{
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		
		try
		{
			txManager.setSessionFactory(sf.getObject());
		}
		catch(Exception e)
		{
			
		}
		
		return txManager;
	}
	
	@Bean
	public DaoForTmd dao()
	{
		return new DaoForTmdImpl();
	}
	
	@Bean
	public RestS3Service s3()
	{
		return new RestS3Service(new AWSCredentials("AKIAIWBWSOSI77PJ73HQ", "2vmqp9pAQHMQky0RojWi2drRmpEQVtN1RJfkVTfe"));
	}
	
	@Bean
	public UserDetailsService userDetailsService()
	{
		return new TmdUserDetailsService();
	}
	
	
}
