package tmd.spring.config;

import java.util.HashSet;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import tmd.dataAccess.DaoForTmd;
import tmd.dataAccess.models.UserTmd;

public class TmdUserDetailsService implements UserDetailsService {

	private DaoForTmd dao;
	@Autowired
	public void setDao(DaoForTmd dao)
	{
		this.dao = dao;
	}
	
	public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException
	{
		
		UserTmd userModel = dao.findUser(user);
		
		if( userModel == null )
			throw new UsernameNotFoundException("Could not locate the user in our records.");
		
		Collection<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ROLE_" + "USER"));
		
		return new User(userModel.getUsername(), userModel.getPassword(), roles);
	}
	
	

}
