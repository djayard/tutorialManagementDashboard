package tmd.spring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class TmdSecurityConfig extends WebSecurityConfigurerAdapter {

	private PasswordEncoder encoder;
	private UserDetailsService userDetails;
	
	@Autowired
	public void setUserDetailsService(UserDetailsService userDetails)
	{
		this.userDetails = userDetails;
	}
	
	@Autowired
	public void setEncoder(PasswordEncoder encoder)
	{
		this.encoder = encoder;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.formLogin().loginPage("/login").usernameParameter("username")
			.passwordParameter("password").successForwardUrl("/service/loginSuccess")
		.and().logout().logoutUrl("/logout")
		.and().authorizeRequests()
			.antMatchers("/","/resources/**", "/login", "/logout").permitAll()
			.antMatchers("/service/**","/s3resources/**").hasRole("USER")
			.anyRequest().permitAll()
		.and().csrf().disable();
		
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetails)
		.passwordEncoder(encoder);
	}
}