package tmd.spring.rest;


import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jets3t.service.ServiceException;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tmd.dataAccess.DaoForTmd;
import tmd.dataAccess.models.*;
import tmd.spring.rest.models.SectionRestModel;
import tmd.spring.rest.models.TutorialRestModel;

@Controller
public class TmdController 
{
	
	private DaoForTmd dao;
	private RestS3Service s3;
	
	@Autowired
	public void setDao(DaoForTmd dao)
	{
		this.dao = dao;
	}
	
	@Autowired
	public void setS3(RestS3Service s3)
	{
		this.s3 = s3;
	}
	
	@RequestMapping(path="login", method=RequestMethod.GET)
	public String welcome()
	{
		return "redirect:/";
	}
	
	@RequestMapping(path="service/loginSuccess", method=RequestMethod.POST)
	public @ResponseBody boolean loginSuccess()
	{
		return true;
	}
	
	@RequestMapping(path="service/tutorials", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody TutorialRestModel[] tutorials()
	{
		return prepareTutorials(dao.getTutorials());
	}
	
	@RequestMapping(path="service/sections", method=RequestMethod.GET, produces="application/json")
	public @ResponseBody SectionRestModel[] sections()
	{
		return prepareSections(dao.getSections());
	}
	
	@RequestMapping(path="service/sectionSearch", method=RequestMethod.GET)
	public @ResponseBody SectionRestModel[] searchSections(@RequestParam String query)
	{
		return prepareSections(dao.searchSections('%' + query + '%'));
	}
	
	@RequestMapping(path="service/tutorialSearch", method=RequestMethod.GET)
	public @ResponseBody TutorialRestModel[] searchTutorials(@RequestParam String query)
	{
		return prepareTutorials(dao.searchTutorials('%' + query + '%'));
	}
	
	

	@RequestMapping(path="service/viewTutorial", method=RequestMethod.GET)
	public void getTutorialView(@RequestParam Integer id, HttpServletRequest req, HttpServletResponse resp)
	{
		try
		{
			String[] s3Info = dao.getS3TutorialInfo(id);
			S3Object obj = s3.getObject(s3Info[0], s3Info[1]);
			
			
			Map<String,Object> metadata = obj.getHttpMetadataMap();
			
			Set<String> keys = metadata.keySet();
			
			for(String k : keys)
			{
				resp.setHeader(k, metadata.get(k).toString());
			}
			
			OutputStream out = resp.getOutputStream();
			InputStream in = obj.getDataInputStream();
			
			byte[] buffer = new byte [1024];
			
			int bytesRead;
			while(  (bytesRead = in.read(buffer)) > -1 )
			{
				out.write(buffer, 0, bytesRead);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path="service/viewSection", method=RequestMethod.GET)
	public void getSectionView(@RequestParam Integer id, HttpServletRequest req, HttpServletResponse resp)
	{
		try
		{
			String[] s3Info = dao.getS3SectionInfo(id);
			S3Object obj = s3.getObject(s3Info[0], s3Info[1]);
			
			
			Map<String,Object> metadata = obj.getHttpMetadataMap();
			
			Set<String> keys = metadata.keySet();
			
			for(String k : keys)
			{
				resp.setHeader(k, metadata.get(k).toString());
			}
			
			OutputStream out = resp.getOutputStream();
			InputStream in = obj.getDataInputStream();
			
			byte[] buffer = new byte [1024];
			
			int bytesRead;
			while(  (bytesRead = in.read(buffer)) > -1 )
			{
				out.write(buffer, 0, bytesRead);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path="/s3resources/**", method=RequestMethod.GET)
	public void getResource(HttpServletRequest req, HttpServletResponse resp)
	{
		final String URI_PREFIX = "/TMD/s3resources/";
		final String RESOURCE_PREFIX = "s3resources/";
		
		String uri = req.getRequestURI();
		uri = uri.replace(URI_PREFIX, "");
		
		String bucketName = uri.substring(0,uri.indexOf('/'));
		String key = RESOURCE_PREFIX + uri;
		
		try
		{
			S3Object obj = s3.getObject(bucketName, key);
			
			
			Map<String,Object> metadata = obj.getHttpMetadataMap();
			
			Set<String> keys = metadata.keySet();
			
			for(String k : keys)
			{
				resp.setHeader(k, metadata.get(k).toString());
			}
			
			OutputStream out = resp.getOutputStream();
			InputStream in = obj.getDataInputStream();
			
			byte[] buffer = new byte [1024];
			
			int bytesRead;
			while(  (bytesRead = in.read(buffer)) > -1 )
			{
				out.write(buffer, 0, bytesRead);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@RequestMapping(path="service/createTutorial", method=RequestMethod.POST)
	public @ResponseBody String createTutorial(@RequestBody Map<String,Object> map)
	{
		try{
		Tutorial tutorial = new Tutorial();
		
		tutorial.setBucket(map.get("bucket").toString());
		tutorial.setFileKey(map.get("fileKey").toString());
		tutorial.setTitle(map.get("title").toString());
		tutorial.setDescription(map.get("description").toString());
		
		
		Set<Tag> tags = new HashSet<Tag>();
		tutorial.setTags(tags);
		StringTokenizer tokenizer = new StringTokenizer(map.get("tags").toString(), ",");
		while( tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken().trim();
			Tag t = new Tag();
			t.setDescription(token);
			tags.add(t);
		}
		
		ArrayList sections = (ArrayList) map.get("sections");
		
		List<Section> sectionsList = new ArrayList(sections.size());
		tutorial.setSections(sectionsList);
		
		for(Object s : sections)
		{
			Section section = new Section();
			section.setId(Integer.parseInt(s.toString()));
			sectionsList.add(section);
		}
		
		tutorial.setCreationDate(new Date());
		tutorial.setLastModifiedDate(new Date());
		
		dao.saveTutorial(tutorial);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		return "success";
	}
	
	@RequestMapping(path="service/createSection", method=RequestMethod.POST)
	public @ResponseBody String createSection(@RequestBody Map<String,Object> map)
	{
		try{
		Section section = new Section();
		
		section.setBucket(map.get("bucket").toString());
		section.setFileKey(map.get("fileKey").toString());
		section.setTitle(map.get("title").toString());
		section.setDescription(map.get("description").toString());
		
		
		Set<Tag> tags = new HashSet<Tag>();
		section.setTags(tags);
		StringTokenizer tokenizer = new StringTokenizer(map.get("tags").toString(), ",");
		while( tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken().trim();
			Tag t = new Tag();
			t.setDescription(token);
			tags.add(t);
		}
		
		section.setCreationDate(new Date());
		section.setLastModifiedDate(new Date());
		
		dao.saveSection(section);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		return "success";
	}
	
	@RequestMapping(path="service/getSection", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> getSection(@RequestParam Integer id)
	{
		Section section = dao.getSection(id);
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy kk:mm:ss aa");
		Map<String,Object> data = new HashMap<String,Object>();
		
		data.put("title", section.getTitle());
		data.put("description", section.getDescription());
		data.put("creationDate", section.getCreationDate());
		data.put("lastModifiedDate", section.getLastModifiedDate());
		data.put("bucket", section.getBucket());
		data.put("fileKey", section.getFileKey());
		
		String tags = "";
		Iterator<Tag> iterator = section.getTags().iterator();
		for(int i = 0; iterator.hasNext(); ++i)
		{
			tags += iterator.next().getDescription() + ", ";
		}
		
		data.put("tags", tags);
		
		return data;
	}
	
	@RequestMapping(path="service/getTutorial", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> getTutorial(@RequestParam Integer id)
	{
		Tutorial tutorial = dao.getTutorial(id);
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy kk:mm:ss aa");
		Map<String,Object> data = new HashMap<String,Object>();
		
		data.put("title", tutorial.getTitle());
		data.put("description", tutorial.getDescription());
		data.put("creationDate", tutorial.getCreationDate());
		data.put("lastModifiedDate", tutorial.getLastModifiedDate());
		data.put("bucket", tutorial.getBucket());
		data.put("fileKey", tutorial.getFileKey());
		
		String tags = "";
		Iterator<Tag> iterator = tutorial.getTags().iterator();
		for(int i = 0; iterator.hasNext(); ++i)
		{
			tags += iterator.next().getDescription() + ", ";
		}
		
		
		
		Map<String,String>[] sections = new HashMap [tutorial.getSections().size()];
		
		for(int i = 0; i < sections.length; ++i)
		{
			sections[i] = new HashMap<String,String>();
			Section section = tutorial.getSections().get(i);
			sections[i].put("id", section.getId().toString());
			sections[i].put("title", section.getTitle());
		}
		
		data.put("tags", tags);
		data.put("sections", sections);
		
		return data;
	}
	
	@RequestMapping(path="service/updateTutorial", method=RequestMethod.POST)
	public @ResponseBody String updateTutorial(@RequestBody Map<String,Object> map)
	{
		try{
		Tutorial tutorial = new Tutorial();
		
		tutorial.setId( Integer.parseInt(map.get("id").toString()));
		tutorial.setBucket(map.get("bucket").toString());
		tutorial.setFileKey(map.get("fileKey").toString());
		tutorial.setTitle(map.get("title").toString());
		tutorial.setDescription(map.get("description").toString());
		tutorial.setLastModifiedDate(new Date());
		
		
		Set<Tag> tags = new HashSet<Tag>();
		tutorial.setTags(tags);
		StringTokenizer tokenizer = new StringTokenizer(map.get("tags").toString(), ",");
		while( tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken().trim();
			if( token.isEmpty() )
				continue;
			
			Tag t = new Tag();
			t.setDescription(token);
			tags.add(t);
		}
		
		ArrayList sections = (ArrayList) map.get("sections");
		
		List<Section> sectionsList = new ArrayList(sections.size());
		tutorial.setSections(sectionsList);
		
		for(Object s : sections)
		{
			Section section = new Section();
			section.setId(Integer.parseInt(s.toString()));
			sectionsList.add(section);
		}
		
		tutorial.setLastModifiedDate(new Date());
		
		dao.updateTutorial(tutorial);
		}catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		return "success";
		
	}
	
	@RequestMapping(path="service/updateSection", method=RequestMethod.POST)
	public @ResponseBody String updateSection(@RequestBody Map<String,Object> map)
	{
		try{
		Section section = new Section();
		
		section.setId( Integer.parseInt(map.get("id").toString()));
		section.setBucket(map.get("bucket").toString());
		section.setFileKey(map.get("fileKey").toString());
		section.setTitle(map.get("title").toString());
		section.setDescription(map.get("description").toString());
		section.setLastModifiedDate(new Date());
		
		
		Set<Tag> tags = new HashSet<Tag>();
		section.setTags(tags);
		StringTokenizer tokenizer = new StringTokenizer(map.get("tags").toString(), ",");
		while( tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken().trim();
			if( token.isEmpty() )
				continue;
			
			Tag t = new Tag();
			t.setDescription(token);
			tags.add(t);
		}
		
		
		section.setLastModifiedDate(new Date());
		
		dao.updateSection(section);
	}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
		return "success";
		
	}
	
	@RequestMapping(path="service/deleteTutorial", method=RequestMethod.DELETE)
	public @ResponseBody TutorialRestModel[] deleteTutorial(@RequestParam Integer id)
	{
		dao.deleteTutorial(id);
		
		return tutorials();
	}
	
	@RequestMapping(path="service/deleteSection", method=RequestMethod.DELETE)
	public @ResponseBody SectionRestModel[] deleteSection(@RequestParam Integer id)
	{
		dao.deleteSection(id);
		
		return sections();
	}
	
	private SectionRestModel[] prepareSections(List<Section> sections)
	{
		SectionRestModel[] model = new SectionRestModel[sections.size()];
		
		for(int i = 0; i < model.length; ++i)
			model[i] = SectionRestModel.loadSection(sections.get(i));
		
		return model;
		
	}
	
	private TutorialRestModel[] prepareTutorials(List<Tutorial> tutorials) 
	{
		TutorialRestModel[] model = new TutorialRestModel[tutorials.size()];
		for(int i = 0; i < model.length; ++i )
		{
			model[i] = TutorialRestModel.loadTutorial(tutorials.get(i));
		}
		
		return model;
	}
}
