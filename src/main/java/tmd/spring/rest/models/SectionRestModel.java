package tmd.spring.rest.models;

import java.io.Serializable;

import tmd.dataAccess.models.Section;
import tmd.dataAccess.models.Tag;

public class SectionRestModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7962450428334005278L;
	private Integer id;
	private String title;
	private String description;
	private String bucket;
	private String fileKey;
	private String tags;
	private Long creationDate;
	private Long lastModifiedDate;
	
	
	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getBucket() {
		return bucket;
	}



	public void setBucket(String bucket) {
		this.bucket = bucket;
	}



	public String getFileKey() {
		return fileKey;
	}



	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}



	public String getTags() {
		return tags;
	}



	public void setTags(String tags) {
		this.tags = tags;
	}



	public Long getCreationDate() {
		return creationDate;
	}



	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}



	public Long getLastModifiedDate() {
		return lastModifiedDate;
	}



	public void setLastModifiedDate(Long lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	static public SectionRestModel loadSection(Section section)
	{
		SectionRestModel model = new SectionRestModel();
		
		model.id = section.getId();
		model.title = section.getTitle();
		model.description = section.getDescription();
		model.bucket = section.getBucket();
		model.fileKey = section.getFileKey();
		
		String tagString = "";
		for(Tag tag: section.getTags())
		{
			tagString += tag.getDescription() + ", ";
		}
		
		if( tagString.endsWith(", ") )
			tagString = tagString.substring(0, tagString.lastIndexOf(", "));
		
		model.tags = tagString;
		
		model.creationDate = section.getCreationDate() == null ? 0 : section.getCreationDate().getTime();
		model.lastModifiedDate = section.getLastModifiedDate() == null ? 0 : section.getLastModifiedDate().getTime();
		
		return model;
	}

}
