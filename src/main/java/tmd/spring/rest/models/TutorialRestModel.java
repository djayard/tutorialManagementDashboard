package tmd.spring.rest.models;

import tmd.dataAccess.models.Tag;
import tmd.dataAccess.models.Tutorial;

public class TutorialRestModel extends SectionRestModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1270186828614039466L;
	private SectionRestModel[] sections;

	public SectionRestModel[] getSections() {
		return sections;
	}


	public void setSections(SectionRestModel[] sections) {
		this.sections = sections;
	}


	static public TutorialRestModel loadTutorial(Tutorial tutorial)
	{
		TutorialRestModel model = new TutorialRestModel();
		
		model.setId(tutorial.getId());
		model.setTitle(tutorial.getTitle());
		model.setDescription(tutorial.getDescription());
		model.setBucket(tutorial.getBucket());
		model.setFileKey(tutorial.getFileKey());
		
		String tagString = "";
		for(Tag tag: tutorial.getTags())
		{
			tagString += tag.getDescription() + ", ";
		}
		
		if( tagString.endsWith(", ") )
			tagString = tagString.substring(0, tagString.lastIndexOf(", "));
		
		model.setTags(tagString);
		
		model.setCreationDate(tutorial.getCreationDate() == null ? 0 : tutorial.getCreationDate().getTime());
		model.setLastModifiedDate(tutorial.getLastModifiedDate() == null ? 0 : tutorial.getLastModifiedDate().getTime());
		
		SectionRestModel[] sections = new SectionRestModel[tutorial.getSections().size()];
		
		for(int i = 0; i < sections.length; ++i)
		{
			sections[i] = SectionRestModel.loadSection(tutorial.getSections().get(i));
		}
		
		model.sections = sections;
		
		return model;
	}
	

}
