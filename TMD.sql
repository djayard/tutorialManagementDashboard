create table TUTORIAL
(
  TUTORIAl_ID number primary key,
  TITLE varchar2(255) not null,
  S3LINK varchar2(255) not null,
  DESCRIPTION varchar2(255),
  DATE_CREATED DATE not null,
  DATE_MODIFIED DATE not null
);

create table SECTION
(
  SECTION_ID number primary key,
  TITLE varchar2(255) not null,
  S3LINK varchar2(255) not null,
  DESCRIPTION varchar2(255),
  DATE_CREATED DATE not null,
  DATE_MODIFIED DATE not null
);

create table TAGS
(
  TAG_ID number primary key,
  DESCRIPTION varchar2(127) not null
);

create table TUTORIAL_TAGS
(
  TUTORIAL_ID number references TUTORIAL(TUTORIAL_ID),
  TAG_ID number references TAGS(TAG_ID),
  primary key(TUTORIAL_ID, TAG_ID)
);

create table SECTION_TAGS
(
  SECTION_ID number references SECTION(SECTION_ID),
  TAG_ID number references TAGS(TAG_ID),
  primary key(SECTION_ID, TAG_ID)
);

create table TMD_USERS
(
  USER_ID number primary key,
  USER_NAME varchar2(50) unique not null,
  USER_PASS varchar2(30) not null
);

create table TUTORIAL_SECTIONS
(
  TUTORIAL_ID number not null references TUTORIAL(TUTORIAL_ID),
  SECTION_ID number not null references SECTION(SECTION_ID,
  primary key(TUTORIAL_ID, SECTION_ID)
);

create sequence TUTORIAL_SEQ start with 1 increment by 1;
create sequence SECTION_SEQ start with 1 increment by 1;
create sequence TAGS_SEQ start with 1 increment by 1;
create sequence USERS_SEQ start with 1 increment by 1;